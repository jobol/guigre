; buf.scm
;
; This is a module for handling binary buffers for guigre
;
; This is the version 0.01.
;
; Features:
;
; Missing:
;
#;(define-module (guigre buf)
   #:export (
      buf:size:init
      buf-create buf-clear
      buf-end buf-bv
      buf-ensured-set!
      buf-set-end!

      buf-set-uint! buf-set-sint!
      buf-set-u8! buf-set-s8!
      buf-set-u16! buf-set-s16!
      buf-set-u32! buf-set-s32!
      buf-set-string!
      buf-set-bytes!

      buf-get-uint buf-get-sint
      buf-get-u8 buf-get-s8
      buf-get-u16 buf-get-s16
      buf-get-u32 buf-get-s32
      buf-get-bytes
      buf-get-column
      buf-get-string
      buf-get-string-list

      set-fields
      @s8 @s16 @s32 @string @blob @s32-blob

      get-fields
      ^s32-blob ^last-blob ^string ^string-list ^s32 ^s16 ^char
      ^s16-columns ^s16-s32s ^id-fields ^s32-strings
   ))

(use-modules (srfi srfi-9))
(use-modules (srfi srfi-11))
(use-modules (rnrs bytevectors))
(use-modules (rnrs bytevectors gnu))

;-----------------------------------------------------
; Some other definitions
;-----------------------------------------------------

; others
(define buf:size:init  16000)
(define BE (endianness big))

;-----------------------------------------------------
;-----------------------------------------------------
;-----------------------------------------------------
; Definitions of <buf> related to buffering protocol
;
;-----------------------------------------------------
;-----------------------------------------------------
;-----------------------------------------------------

;;; the record type <buf> holds
;;; - base offset of the U32 encoding the message length
;;; - end  the length of the data in 'bv'
;;; - bv   this is the bytevector holding protocols data
(define-record-type <buf>
   (!buf! end bv)
   buf?
   (end  buf-end  set-buf-end!)
   (bv   buf-bv   set-buf-bv!))

;;; creation of a buf with initial size
(define (buf-create size)
   (!buf! 0 (make-bytevector size)))

;;; reset the content of the buffer
(define (buf-clear buf)
   (set-buf-end! buf 0))

;;; ensure that the buffer 'buf' is big enough to hold 'len' bytes
;;; then call proc with the corresponding bytevector
;;; and return its value
(define (buf-ensured-set! buf len proc)
   ; get the length of the bytevector that buf holds
   (let* ((bv    (buf-bv buf))
          (bvlen (bytevector-length bv)))
      ; check if enough room exists?
      (if (<= len bvlen)
         ; yes, so call proc with current bv
         (proc bv)
         ; no, so create a big enougth copy and call proc on it
         (let* ((new-len (max len (+ bvlen bvlen)))
                (new-bv  (make-bytevector new-len 0)))
            (bytevector-copy! new-bv 0 bv 0 bvlen)
            (set-buf-bv! buf new-bv)
            (proc new-bv)))))

;;; update end value of 'buf' and return it
(define (buf-set-end! buf end)
   (set-buf-end! buf end)
   end)

;-----------------------------------------------------
; setting primitive values in buf
;-----------------------------------------------------

;;; set in 'buf' the byte 'value' of 'size' byte(s)
;;; in big endian at the 'index'
;;; return index of the byte after (index + size)
;;; this function does not change the value of buf-end
(define (buf-set-uint! buf index value size)
   (buf-ensured-set!
         buf
         (+ index size)
         (lambda (bv)
            (bytevector-uint-set! bv index value BE size)
            (+ index size))))

;;; set in 'buf' the byte 'value' of 'size' byte(s)
;;; in big endian at the 'index'
;;; return index of the byte after (index + size)
;;; this function does not change the value of buf-end
(define (buf-set-sint! buf index value size)
   (buf-ensured-set!
         buf
         (+ index size)
         (lambda (bv)
            (bytevector-sint-set! bv index value BE size)
            (+ index size))))

;;; set in 'buf' the byte 'value' at the 'index'
;;; return index of the byte after (index + 1)
;;; this function does not change the value of buf-end
(define (buf-set-u8! buf index value)
   (buf-set-uint! buf index value 1))

;;; set in 'buf' the byte 'value' at the 'index'
;;; return index of the byte after (index + 1)
;;; this function does not change the value of buf-end
(define (buf-set-s8! buf index value)
   (buf-set-sint! buf index value 1))

;;; set in 'buf' the byte 'value' at the 'index'
;;; return index of the byte after (index + 2)
;;; this function does not change the value of buf-end
(define (buf-set-u16! buf index value)
   (buf-set-uint! buf index value 2))

;;; set in 'buf' the byte 'value' at the 'index'
;;; return index of the byte after (index + 2)
;;; this function does not change the value of buf-end
(define (buf-set-s16! buf index value)
   (buf-set-sint! buf index value 2))

;;; set in 'buf' the byte 'value' at the 'index'
;;; return index of the byte after (index + 4)
;;; this function does not change the value of buf-end
(define (buf-set-u32! buf index value)
   (buf-set-uint! buf index value 4))

;;; set in 'buf' the byte 'value' at the 'index'
;;; return index of the byte after (index + 4)
;;; this function does not change the value of buf-end
(define (buf-set-s32! buf index value)
   (buf-set-sint! buf index value 4))

;;; set in 'buf' the utf8 encoding of string 'value' at the 'index'
;;; also set a trailing null
;;; return index of the byte after (index + utf8-length + 1)
;;; this function does not change the value of buf-end
(define (buf-set-string! buf index value)
   (let* ((strbv (string->utf8 value))
          (sbvl  (bytevector-length strbv))
          (posz  (+ index sbvl))
          (end   (+ posz 1)))
      (buf-ensured-set!
            buf
            end
            (lambda (bv)
               (bytevector-copy! strbv 0 bv index sbvl)
               (bytevector-u8-set! bv posz 0)
               end))))

;;; set in 'buf' the value of the blob 'value' at 'index'
;;; return that value and the index after the blob
;;; this function does not change the value of buf-end
(define (buf-set-bytes! buf index value)
   (let* ((len  (bytevector-length value))
          (end  (+ index len)))
      (buf-ensured-set!
            buf
            end
            (lambda (bv)
               (bytevector-copy! value 0 bv index len)
               end))))

;-----------------------------------------------------
; getting primitive values from buf
;-----------------------------------------------------

;;; get from 'buf' the unsigned value of 'size' byte(s)
;;; in big endian at the 'index'
;;; return 2 values: the value it got and the index of
;;; the byte after (index + size)
(define (buf-get-uint buf index size)
   (values (bytevector-uint-ref (buf-bv buf) index BE size)
           (+ index size)))

;;; get from 'buf' the signed value of 'size' byte(s)
;;; in big endian at the 'index'
;;; return 2 values: the value it got and the index of
;;; the byte after (index + size)
(define (buf-get-sint buf index size)
   (values (bytevector-sint-ref (buf-bv buf) index BE size)
           (+ index size)))

;;; get from 'buf' the value of the unsigned byte at 'index'
;;; return that value and (index + 1)
(define (buf-get-u8 buf index)
   (buf-get-uint buf index 1))

;;; get from 'buf' the value of the signed byte at 'index'
;;; return that value and (index + 1)
(define (buf-get-s8 buf index)
   (buf-get-sint buf index 1))

;;; get from 'buf' the value of the unsigned 2 bytes word at 'index'
;;; return that value and (index + 2)
(define (buf-get-u16 buf index)
   (buf-get-uint buf index 2))

;;; get from 'buf' the value of the signed 2 bytes word at 'index'
;;; return that value and (index + 2)
(define (buf-get-s16 buf index)
   (buf-get-sint buf index 2))

;;; get from 'buf' the value of the unsigned 4 bytes word at 'index'
;;; return that value and (index + 4)
(define (buf-get-u32 buf index)
   (buf-get-uint buf index 4))

;;; get from 'buf' the value of the signed 4 bytes word at 'index'
;;; return that value and (index + 4)
(define (buf-get-s32 buf index)
   (buf-get-sint buf index 4))

;;; get from 'buf' the value of the blob of 'len' at 'index'
;;; return that value and the index after the blob
(define (buf-get-bytes buf index len)
   (values (bytevector-copy (bytevector-slice (buf-bv buf) index len))
           (+ index len)))

;;; get from 'buf' the value of the column at 'index'
;;; return that value and the index after the blob
(define (buf-get-column buf index)
   (let-values (((len nxt) (buf-get-s32 buf index)))
      (if (negative? len)
         (values 'NULL nxt)
         (buf-get-bytes buf nxt len))))
 
;;; get from 'buf' the value of the null terminated string at 'index'
;;; return that value and the index after the string
(define (buf-get-string buf index)
   (let ((bv (buf-bv buf)))
      (let loop ((idx index))
         (if (not (zero? (bytevector-u8-ref bv idx)))
            (loop (+ idx 1))
            (values
               (utf8->string (bytevector-slice bv index (- idx index)))
               (+ idx 1))))))

;;; get from 'buf' the string list, a sequence of string
;;; starting at 'index' terminated by an empty string
;;; return that list of strings and the index after the list
(define (buf-get-string-list buf index)
   (let-values (((str nxt) (buf-get-string buf index)))
      (if (zero? (string-length str))
         (values '() nxt)
         (let-values (((strlst end) (buf-get-string-list buf nxt)))
            (values (cons str strlst) end)))))

;-----------------------------------------------------
;-----------------------------------------------------

(define-syntax set-fields
   (syntax-rules ()
      ((_ buf pos)
         pos)
      ((_ buf pos writer value . rest)
         (let ((nxt (writer buf pos value)))
            (set-fields buf nxt . rest)))
      ((_ ...) (syntax-error "invalid set-fields"))))

(define-syntax @s8
   (syntax-rules ()
      ((_ buf pos val)
         (buf-set-s8! buf pos val))))

(define-syntax @s16
   (syntax-rules ()
      ((_ buf pos val)
         (buf-set-s16! buf pos val))))

(define-syntax @s32
   (syntax-rules ()
      ((_ buf pos val)
         (buf-set-s32! buf pos val))))

(define-syntax @string
   (syntax-rules ()
      ((_ buf pos val)
         (buf-set-string! buf pos val))))

(define-syntax @blob
   (syntax-rules ()
      ((_ buf pos val)
         (buf-set-bytes! buf pos val))))

(define-syntax @s32-blob
   (syntax-rules ()
      ((_ buf pos val)
         (let ((len (bytevector-length val)))
            (let ((pos (buf-set-s32! buf pos len)))
               (buf-set-bytes! buf pos val))))))

;-----------------------------------------------------
;-----------------------------------------------------

(define-syntax get-fields
   (syntax-rules ()
      ((_ buf pos end)
         (values '() pos))
      ((_ buf pos end key reader . rest)
         (let-values (((val nxt) (reader buf pos end)))
            (let-values (((ovals lst) (get-fields buf nxt end . rest)))
               (values (cons (cons key val) ovals) lst))))
      ((_ ...) (syntax-error "invalid get-fields"))))


(define-syntax ^s32-blob
   (syntax-rules ()
      ((_ buf pos end)
         (let-values (((len nxt) (buf-get-s32 buf pos)))
            (buf-get-bytes buf nxt len)))))

(define-syntax ^last-blob
   (syntax-rules ()
      ((_ buf pos end)
         (buf-get-bytes buf pos (- end pos)))))

(define-syntax ^string
   (syntax-rules ()
      ((_ buf pos end)
         (buf-get-string buf pos))))

(define-syntax ^string-list
   (syntax-rules ()
      ((_ buf pos end)
         (buf-get-string-list buf pos))))

(define-syntax ^s32
   (syntax-rules ()
      ((_ buf pos end)
         (buf-get-s32 buf pos))))

(define-syntax ^s16
   (syntax-rules ()
      ((_ buf pos end)
         (buf-get-s16 buf pos))))

(define-syntax ^char
   (syntax-rules ()
      ((_ buf pos end)
         (let-values (((val nxt) (buf-get-u8 buf pos)))
            (values (integer->char val) nxt)))))

(define-syntax ^s16-columns
   (syntax-rules ()
      ((_ buf pos end)
         (let-values (((count nxt) (buf-get-s16 buf pos)))
            (let loop ((dcpt count) (lst '()) (pos nxt))
               (if (zero? dcpt)
                  (values (reverse lst) pos)
                  (let-values (((val nxt) (buf-get-column buf pos)))
                     (loop (- dcpt 1) (cons val lst) nxt))))))))

(define-syntax ^s16-s32s
   (syntax-rules ()
      ((_ buf pos end)
         (let-values (((count nxt) (buf-get-s16 buf pos)))
            (let loop ((dcpt count) (lst '()) (pos nxt))
               (if (zero? dcpt)
                  (values (reverse lst) pos)
                  (let-values (((val nxt) (buf-get-s32 buf pos)))
                     (loop (- dcpt 1) (cons val lst) nxt))))))))

(define-syntax ^id-fields
   (syntax-rules ()
      ((_ buf pos end)
         (let loop ((lst '()) (pos pos))
            (let-values (((code nxt) (buf-get-u8 buf pos)))
               (if (zero? code)
                  (values (reverse lst) nxt)
                  (let-values (((val nxt) (buf-get-string buf nxt)))
                     (loop (cons (cons (integer->char code) val) lst) nxt))))))))

(define-syntax ^s32-strings
   (syntax-rules ()
      ((_ buf pos end)
         (lambda (buf pos end)
            (let-values (((count nxt) (buf-get-s32 buf pos)))
               (let loop ((dcpt count) (lst '()) (pos nxt))
                  (if (zero? dcpt)
                     (values (reverse lst) pos)
                     (let-values (((val nxt) (buf-get-string buf pos)))
                        (loop (- dcpt 1) (cons val lst) nxt)))))))))

;; vim: noai ts=3 sw=3 expandtab

