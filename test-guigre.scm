
(include "guigre.scm")

;-----------------------------------------------------
;-----------------------------------------------------
;-----------------------------------------------------
;-----------------------------------------------------
;-----------------------------------------------------
; for testing
;-----------------------------------------------------

; create a cnx from a connected socket
(define (make-cnx-sock sock)
   (make-cnx
      (lambda (bv start count)
         (get-bytevector-n! sock bv start count))
      (lambda (bv start count)
         (put-bytevector sock bv start count))
      (lambda ()
         (shutdown sock 2))))

; create a cnx from a socket address
(define (make-cnx-sockaddr saddr)
   (let ((sock (socket (sockaddr:fam saddr) SOCK_STREAM 0)))
      (connect sock saddr)
      (make-cnx-sock sock)))

; create a cnx from a ipv4 server and port
(define (make-cnx-ipv4 host service-or-port)
   (let* ((sp    (if (number? service-or-port)
                     (number->string service-or-port)
                     service-or-port))
          (haddr (car (getaddrinfo host sp 0 PF_INET)))
          (saddr (addrinfo:addr haddr)))
      (make-cnx-sockaddr saddr)))

; create a cnx from a ipv6 server and port
(define (make-cnx-ipv6 host service-or-port)
   (let* ((haddr (car (getaddrinfo host #f 0 PF_INET6)))
          (saddr (make-socket-address AF_INET6 haddr port)))
      (make-cnx-sockaddr saddr)))

; create a cnx from a unix path server
(define (make-cnx-unix path)
   (let ((saddr (make-socket-address AF_UNIX path)))
      (make-cnx-sockaddr saddr)))

(define (debug . args)
   (for-each display args)
   (newline))

(dump-received-messages (current-output-port))

(define params '(("user" . "jobol")
                 ("database" . "arene")
                 ("application_name" . "guigre")
                 ("client_encoding" . "UTF8")))

(define host  "localhost")
(define port  5432)
(password "rien")

(define p-cnx (make-cnx-ipv4 host port))
(define sts   (startup-cnx p-cnx params))



(define sql-init 
   "CREATE COLLATION IF NOT EXISTS nocase (
      provider = icu,
      locale = 'und-u-ks-level2',
      deterministic = false
   );
   CREATE TABLE IF NOT EXISTS edt (
      jour CHAR(8) NOT NULL,
      qui  VARCHAR(20) NOT NULL COLLATE nocase,
      quand TEXT,
      PRIMARY KEY (jour, qui)
   );")

(define sql-add
   "INSERT INTO edt VALUES($1,$2,$3)
      ON CONFLICT (jour, qui) DO UPDATE SET quand = $3;")

(define sql-get
   "SELECT ALL jour, qui, quand FROM edt
      WHERE jour >= $1 AND jour <= $2;")

(debug "*****1******")
(query p-cnx sql-init (lambda (row seed) seed) #t)

(debug "*****2******")
(define (get-table name)
   (reverse (query p-cnx
               (string-append "SELECT ALL * FROM " name)
               (row-string cons)
               '())))

(debug "*****3******")
(debug "TABLE edt:" (get-table "edt"))

#|
(debug "*****4******")
(parse p-cnx "get" sql-get)
(parse p-cnx "add" sql-add)

(debug "*****5******")
(bind p-cnx "get" "get" (list "20250101" "20251231"))

(debug "*****6******")
(execute p-cnx "get" 0 (row-string cons) '())

(debug "*****7******")
(bindexec p-cnx "add" (list "20250101" "josé" "0900-1800") #f #f)
(bindexec p-cnx "add" (list "20250102" "josé" "0900-1800") #f #f)
(bindexec p-cnx "add" (list "20250103" "josé" "0900-1800") #f #f)
(bindexec p-cnx "add" (list "20250105" "josé" "0900-1800") #f #f)

(debug "*****8******")
(define (get-range from to)
   (reverse (bindexec p-cnx "get" (list from to) (row-string cons) '())))
(debug "range :" (get-range "20200101" "20251231"))
|#
(debug "*****9******")
(cnx-shutdown p-cnx)
(debug "***********")

;; vim: noai ts=3 sw=3 expandtab


