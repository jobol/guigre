; guigre.scm
;
; This is a guile module for communicating with a POSTGRESQL server
; using the Frontend/Backend Protocol as described by
; https://www.postgresql.org/docs/current/protocol.html
;
; This is the version 0.01.
;
; Features:
;
; Missing:
;

(use-modules (srfi srfi-1))
(use-modules (srfi srfi-9))
(use-modules (srfi srfi-11))
(use-modules (rnrs bytevectors))
(use-modules (ice-9 binary-ports))

;(use-modules (guigre buf))
(include "buf.scm")

;-----------------------------------------------------
; Codes used by the protocol
;-----------------------------------------------------

(define code:ProtocolV3.0        #x30000)

; These are the request codes sent by the frontend.

(define code:Bind                  #\B)
(define code:Close                 #\C)
(define code:Describe              #\D)
(define code:Execute               #\E)
(define code:FunctionCall          #\F)
(define code:Flush                 #\H)
(define code:Parse                 #\P)
(define code:Query                 #\Q)
(define code:Sync                  #\S)
(define code:Terminate             #\X)
(define code:CopyFail              #\f)
(define code:GSSResponse           #\p)
(define code:PasswordMessage       #\p)
(define code:SASLInitialResponse   #\p)
(define code:SASLResponse          #\p)

; These are the response codes sent by the backend.

(define code:ParseComplete         #\1)
(define code:BindComplete          #\2)
(define code:CloseComplete         #\3)
(define code:NotificationResponse  #\A)
(define code:CommandComplete       #\C)
(define code:DataRow               #\D)
(define code:ErrorResponse         #\E)
(define code:CopyInResponse        #\G)
(define code:CopyOutResponse       #\H)
(define code:EmptyQueryResponse    #\I)
(define code:BackendKeyData        #\K)
(define code:NoticeResponse        #\N)
(define code:AuthenticationRequest #\R)
(define code:ParameterStatus       #\S)
(define code:RowDescription        #\T)
(define code:FunctionCallResponse  #\V)
(define code:CopyBothResponse      #\W)
(define code:ReadyForQuery         #\Z)
(define code:NoData                #\n)
(define code:PortalSuspended       #\s)
(define code:ParameterDescription  #\t)
(define code:NegotiateProtocolVersion  #\v)

; These are the codes sent by both the frontend and backend.

(define code:CopyDone              #\c)
(define code:CopyData              #\d)

; These are the authentication request codes sent by the backend.

(define code:auth:OK            0)   ; User is authenticated
(define code:auth:KRB4          1)   ; Kerberos V4. Not supported any more.
(define code:auth:KRB5          2)   ; Kerberos V5. Not supported any more.
(define code:auth:PASSWORD      3)   ; Password
(define code:auth:CRYPT         4)   ; crypt password. No more supported.
(define code:auth:MD5           5)   ; md5 password
(define code:auth:GSS           7)   ; GSSAPI without wrap()
(define code:auth:GSS_CONT      8)   ; Continue GSS exchanges
(define code:auth:SSPI          9)   ; SSPI negotiate without wrap()
(define code:auth:SASL         10)   ; Begin SASL authentication
(define code:auth:SASL_CONT    11)   ; Continue SASL authentication
(define code:auth:SASL_FIN     12)   ; Final SASL message

;-----------------------------------------------------
;-----------------------------------------------------

(define-syntax message-encode
   (syntax-rules ()
      ((_ buf pos . rest)
         (let ((end (set-fields buf (+ pos 4) . rest)))
            (buf-set-s32! buf pos (- end pos))
            (buf-set-end! buf end)))
      ((_ ...)
         (syntax-error "invalid message"))))

(define-syntax simple-message-encode
   (syntax-rules ()
      ((_ buf code . rest)
         (let* ((pos (buf-end buf))
                (val (char->integer code))
                (pos (buf-set-u8! buf pos val)))
            (message-encode buf pos . rest)))))

(define-syntax @param-list
   (syntax-rules ()
      ((_ buf pos val)
         (let loop ((p0 pos) (l val))
            (if (null? l)
               (buf-set-s8! buf p0 0)
               (let* ((v  (car l))
                      (p1 (buf-set-string! buf p0 (car v)))
                      (p2 (buf-set-string! buf p1 (cdr v))))
                  (loop p2 (cdr l))))))))

;-----------------------------------------------------
;-----------------------------------------------------


;-----------------------------------------------------
;-----------------------------------------------------

;;; check validity of startup params
;;; a valid startup parms is a list string pairs
;;; (("key" . "value") ...) containing at least
;;; the key "user"
(define (valid-startup-params? params)
   (and (list? params)
        (every (lambda (x) (and (pair? x)
                                (string? (car x))
                                (string? (cdr x))))
               params)
        (assoc "user" params)))

;;; startup the connection
;;; encode in buf the startup message
(define (msg-code-Startup! buf params)
   ; check params
   (unless (valid-startup-params? params)
      (error "invalid startup params" params))
   ; compose startup message, it has a special format
   (message-encode buf 0
      @s32 code:ProtocolV3.0
      @param-list params))

;;; check validity of startup params
(define (valid-query? query)
   (string? query))

;
(define (msg-code-Sync! buf)
   ; compose query message
   (simple-message-encode buf
      code:Sync))

;
(define (msg-code-Execute! buf portal count)
   ; compose query message
   (simple-message-encode buf
      code:Execute
         @string portal
         @s32 count))

;
(define (msg-code-Query! buf query)
   ; check params
   (unless (valid-query? query)
      (error "invalid query" query))
   ; compose query message
   (simple-message-encode buf
      code:Query
         @string query))

(define (msg-code-PasswordMessage! buf password)
   (simple-message-encode buf
      code:PasswordMessage
         @string password))
   
;
(define (msg-code-Parse! buf name query)
   ; check params
   (unless (valid-query? query)
      (error "invalid query" query))
   ; compose query message
   (simple-message-encode buf
      code:Parse
         @string name
         @string query
         @s16    0))

(define (msg-code-Bind! buf portal name params)
   (simple-message-encode buf
      code:Bind
         @string portal
         @string name
         @s16 0
         @s16 (length params)
         (lambda (buf pos lst)
            (let loop ((pos pos) (lst lst))
               (if (null? lst)
                  pos
                  (let* ((str (car lst))
                         (bv  (string->utf8 str))
                         (len (bytevector-length bv))
                         (pos (buf-set-s32! buf pos len))
                         (pos (buf-set-bytes! buf pos bv)))
                     (loop pos (cdr lst)))))) params
         @s16 0))


;-----------------------------------------------------
;-----------------------------------------------------


(define (assval key lst)
   (cond
      ((assoc key lst) => cdr)
      (else #f)))

(define (set-assval! key lst val)
   (let ((pair (assoc key lst)))
      (when pair
         (set-cdr! pair val))))





(define (make-msg id datum)
   (cons id datum))

(define (msg-tag msg)
   (car msg))

(define (msg-datum msg)
   (cdr msg))

(define (msg-item msg key)
   (assval key (msg-datum msg)))

(define (set-msg-item! msg key val)
   (set-assval! key (msg-datum msg) val))







(define (unimpl tag)
   (lambda _ (error "unimplemented message decoder" tag)))




(define-syntax message-decode-reader
   (syntax-rules ()
      ((_ buf pos end id . rest)
         (let-values (((val nxt) (get-fields buf pos end . rest)))
            (when (not (= nxt end))
               (error "mismatch end" id "expected:" end "got:" nxt "pos:" pos))
            (values (make-msg id val) nxt)))
      ((_ ...)
         (syntax-error "invalid message"))))

(define-syntax simple-message-decoder
   (syntax-rules ()
      ((_ id . rest)
         (lambda (buf pos end)
            (message-decode-reader buf pos end id . rest)))))

(define-syntax complex-i32-message-decoder
   (syntax-rules ()
      ((_ (code id . args) ...)
         (let ((defs (list (cons code (lambda (buf pos end)
                                          (message-decode-reader buf pos end id . args))) ...)))
            (lambda (buf pos end)
               (let-values (((tag nxt) (buf-get-u32 buf pos)))
                  (let ((entry (assoc tag defs)))
                     (if entry
                        ((cdr entry) buf nxt end)
                        (error "unexpected message" tag)))))))))















(define decode-message-AuthenticationRequest
   (complex-i32-message-decoder
      (code:auth:OK
            'AuthenticationOk)
      (code:auth:PASSWORD
            'AuthenticationCleartextPassword)
      (code:auth:MD5
            'AuthenticationMD5Password
               'salt ^last-blob)
      (code:auth:SASL
            'AuthenticationSASL
               'mechanisms ^string-list)
      (code:auth:SASL_CONT
            'AuthenticationSASLContinue
               'data ^last-blob)
      (code:auth:SASL_FIN
            'AuthenticationSASLFinal
               'data ^last-blob)))

(define decode-message-BackendKeyData
   (simple-message-decoder
      'BackendKeyData
         'process-id ^s32
         'secret-key ^s32))

(define decode-message-BindComplete
   (simple-message-decoder
      'BindComplete))

(define decode-message-CloseComplete
   (simple-message-decoder
      'CloseComplete))

(define decode-message-CommandComplete
   (simple-message-decoder
      'CommandComplete
         'tag ^string))

(define (decode-message-CopyBothResponse buf len)
   (unimpl 'CopyBothResponse))

(define decode-message-CopyData
   (simple-message-decoder
      'CopyData
         'data ^last-blob))

(define decode-message-CopyDone
   (simple-message-decoder
      'CopyDone))

(define decode-message-CopyInResponse
   (unimpl 'CopyInResponse))

(define decode-message-CopyOutResponse
   (unimpl 'CopyOutResponse))

(define decode-message-DataRow
   (simple-message-decoder
      'DataRow
         'columns ^s16-columns))

(define decode-message-EmptyQueryResponse
   (simple-message-decoder
      'EmptyQueryResponse))

(define decode-message-ErrorResponse
   (simple-message-decoder
      'ErrorResponse
         'fields ^id-fields))

(define decode-message-FunctionCallResponse
   (simple-message-decoder
      'FunctionCallResponse
         'result ^s32-blob))

(define decode-message-NegotiateProtocolVersion
   (simple-message-decoder
      'NegotiateProtocolVersion
         'newer-minor ^s32
         'unrecognized-option-names ^s32-strings))

(define decode-message-NoData
   (simple-message-decoder
      'NoData))

(define decode-message-NoticeResponse
   (simple-message-decoder
      'NoticeResponse
         'fields ^id-fields))

(define decode-message-NotificationResponse
   (simple-message-decoder
      'NotificationResponse
         'process-id ^s32
         'channel-name ^string
         'payload ^string))

(define decode-message-ParameterDescription
   (simple-message-decoder
      'ParameterDescription
         'object-ids ^s16-s32s))

(define decode-message-ParameterStatus
   (simple-message-decoder
      'ParameterStatus
         'name ^string
         'value ^string))

(define decode-message-ParseComplete
   (simple-message-decoder
      'ParseComplete))

(define decode-message-PortalSuspended
   (simple-message-decoder
      'PortalSuspended))

(define decode-message-ReadyForQuery
   (simple-message-decoder
      'ReadyForQuery
         'status ^char))

(define decode-message-RowDescription
   (simple-message-decoder
      'RowDescription
         'columns
            (lambda (buf pos end)
               (let-values (((count nxt) (buf-get-s16 buf pos)))
                  (let loop ((dcpt count) (lst '()) (pos nxt))
                     (if (zero? dcpt)
                        (values (reverse lst) pos)
                        (let-values (((val nxt) (get-fields buf pos end
                                                   'name ^string
                                                   'table-id ^s32
                                                   'field-id ^s16
                                                   'type-id ^s32
                                                   'type-size ^s16
                                                   'type-modif ^s32
                                                   'format ^s16)))
                           (loop (- dcpt 1) (cons val lst) nxt))))))))

(define dump-received-messages
   (make-parameter #f (lambda (x) (and (port? x) x))))

(define decode-message
   (let ((decoders (list
               (cons code:AuthenticationRequest decode-message-AuthenticationRequest)
               (cons code:BackendKeyData       decode-message-BackendKeyData)
               (cons code:BindComplete         decode-message-BindComplete)
               (cons code:CloseComplete        decode-message-CloseComplete)
               (cons code:CommandComplete      decode-message-CommandComplete)
               (cons code:CopyBothResponse     decode-message-CopyBothResponse)
               (cons code:CopyData             decode-message-CopyData)
               (cons code:CopyDone             decode-message-CopyDone)
               (cons code:CopyInResponse       decode-message-CopyInResponse)
               (cons code:CopyOutResponse      decode-message-CopyOutResponse)
               (cons code:DataRow              decode-message-DataRow)
               (cons code:EmptyQueryResponse   decode-message-EmptyQueryResponse)
               (cons code:ErrorResponse        decode-message-ErrorResponse)
               (cons code:FunctionCallResponse decode-message-FunctionCallResponse)
               (cons code:NegotiateProtocolVersion decode-message-NegotiateProtocolVersion)
               (cons code:NoData               decode-message-NoData)
               (cons code:NoticeResponse       decode-message-NoticeResponse)
               (cons code:NotificationResponse decode-message-NotificationResponse)
               (cons code:ParameterDescription decode-message-ParameterDescription)
               (cons code:ParameterStatus      decode-message-ParameterStatus)
               (cons code:ParseComplete        decode-message-ParseComplete)
               (cons code:PortalSuspended      decode-message-PortalSuspended)
               (cons code:ReadyForQuery        decode-message-ReadyForQuery)
               (cons code:RowDescription       decode-message-RowDescription))))
      (lambda (buf)
         (let-values (((key beg) (buf-get-u8 buf 0)))
            (let* ((code (integer->char key))
                   (pdec (assoc code decoders)))
               (unless pdec
                  (error "unexpected message code" code))
               (let*-values (((len con) (buf-get-u32 buf beg))
                             ((msg end) ((cdr pdec) buf con (+ beg len))))
                  (when (dump-received-messages)
                     (display "MSG: " (dump-received-messages))
                     (display msg (dump-received-messages))
                     (newline (dump-received-messages)))
                  msg))))))


;-----------------------------------------------------
;-----------------------------------------------------
;-----------------------------------------------------
; Definition of <cnx> the socket connection to PostgreSQL
;-----------------------------------------------------
;-----------------------------------------------------
;-----------------------------------------------------

;-------- cnx ----------------------------
;;; the record <cnx> holds
;;; - port the socket connection
;;; - in   the input buffer
;;; - out  the output buffer
(define-record-type <cnx>
   (!cnx! in-buf out-buf read write close)
   cnx?
   (in-buf  cnx-in-buf)
   (out-buf cnx-out-buf)
   (read    %cnx-read)
   (write   %cnx-write)
   (close   %cnx-close))

; create a cnx from a connected socket
(define (make-cnx read write close)
   (!cnx! (buf-create buf:size:init)
          (buf-create buf:size:init)
          read write close))

; shutdown the connection
(define (cnx-shutdown cnx)
   ((%cnx-close cnx)))

;-----------------------------------------------------
; sending
;-----------------------------------------------------

; can send the output buffer?
(define (cnx-can-send? cnx)
   (let* ((buf  (cnx-out-buf cnx))
          (len  (buf-end buf)))
      (positive? len)))

; send the output buffer on the connexion
; then clear it
(define (cnx-send cnx)
   (let* ((write (%cnx-write cnx))
          (buf   (cnx-out-buf cnx))
          (end   (buf-end buf)))
      (unless (zero? end)
         (write (buf-bv buf) 0 end)
         (buf-clear buf))))

(define (buf-receive buf pos count read)
   (if (zero? count)
      pos
      (let ((nxt (+ pos count)))
         (buf-ensured-set!
            buf
            nxt
            (lambda (bv)
               (let ((rc (read bv pos count)))
                  (unless (= rc count)
                     (error "read error"))
                  nxt))))))

;;; receive one message from backend
(define (cnx-receive cnx)
   (let ((read (%cnx-read  cnx))
         (buf  (cnx-in-buf cnx)))
      (buf-clear buf)
      (buf-receive buf 0 5 read)
      (buf-receive buf 5 (- (buf-get-u32 buf 1) 4) read)))

(define (cnx-get-msg cnx)
   (when (cnx-can-send? cnx)
      (cnx-send cnx))
   (cnx-receive cnx)
   (decode-message (cnx-in-buf cnx)))

;-----------------------------------------------------
;-----------------------------------------------------

(define (cnx-msg-loop cnx proc seed)
   (let next ((seed seed))
      (let ((msg (cnx-get-msg cnx)))
         (proc msg next seed))))

(define (cnx-until-ready cnx proc seed)
   (let ((until (lambda (msg next seed)
                  (if (eqv? (msg-tag msg) 'ReadyForQuery)
                     seed
                     (next (proc msg seed))))))
      (cnx-msg-loop cnx until seed)))

(define (cnx-wait-ready cnx)
   (let ((nop (lambda (msg seed) seed)))
      (cnx-until-ready cnx nop #t)))

(define (filter-row proc)
   (lambda (msg seed)
      (if (and proc (eqv? (msg-tag msg) 'DataRow))
         (proc (msg-item msg 'columns) seed)
         seed)))

(define (row-string proc)
   (lambda (row seed)
      (proc (map utf8->string row) seed)))

;-----------------------------------------------------
;-----------------------------------------------------
; 53.1.1

; section 53.2.2

(define password (make-parameter "**unset**"))

;;; startup the connection
(define (startup-cnx cnx params)
   ; compose startup message
   (msg-code-Startup! (cnx-out-buf cnx) params)
   ; process
   (let loop ()
      (let ((msg (cnx-get-msg cnx)))
         (case (msg-tag msg)
            ((AuthenticationOk)
               (cnx-wait-ready cnx))
            ((AuthenticationCleartextPassword)
               (msg-code-PasswordMessage! (cnx-out-buf cnx) (password))
               (loop))
   ;         ((AuthenticationMD5Password)
   ;            #f)
   ;         ((AuthenticationSASL)
   ;            #f)
   ;         ((AuthenticationSASLContinue)
   ;            #f)
   ;         ((AuthenticationSASLFinal)
   ;            #f)
   ;         ((ErrorResponse)
   ;            #f)
            (else
               (error "..."))))))

; make a query (or a list of queries) and call proc
; with the rows.
;
; (query cnx query proc seed) -> next-seed
;
;   cnx := CNX
;   query := STRING
;   proc := (proc row seed) -> next-seed
;   seed := ANY 
;
(define (query cnx query proc seed)
   (msg-code-Query! (cnx-out-buf cnx) query)
   (cnx-until-ready cnx (filter-row proc) seed))


; prepare a named statement
;
; (parse cnx name statement) -> void
;
;   cnx := CNX
;   name := STRING
;   statement := STRING
;
(define (parse cnx name statement)
   (msg-code-Parse! (cnx-out-buf cnx) name statement)
   (msg-code-Sync! (cnx-out-buf cnx))
   (cnx-wait-ready cnx))

; bind parameters to a named statement for a named portal
;
; (bind cnx portal name params) -> void
;
;   cnx := CNX
;   portal := STRING
;   name := STRING
;   params := (STRING ...)
;
(define (bind cnx portal name params)
   (msg-code-Bind! (cnx-out-buf cnx) portal name params)
   (msg-code-Sync! (cnx-out-buf cnx))
   (cnx-wait-ready cnx))
   
; execute the named portal for limited to count results
; (0 for no limit)
;
; (execute cnx portal count proc seed) -> next-seed
;
;   cnx := CNX
;   portal := STRING
;   count := INTEGER
;   proc := (proc row seed) -> next-seed
;   seed := ANY 
;
(define (execute cnx portal count proc seed)
   (msg-code-Execute! (cnx-out-buf cnx) portal count)
   (msg-code-Sync! (cnx-out-buf cnx))
   (cnx-until-ready cnx (filter-row proc) seed))

; bind parameters to a named statement and execute it
;
; (bindexec cnx name params proc seed) -> next-seed
;
;   cnx := CNX
;   name := STRING
;   params := (STRING ...)
;   proc := (proc row seed) -> next-seed
;   seed := ANY 
;
(define (bindexec cnx name params proc seed)
   (define portal "")
   (msg-code-Bind! (cnx-out-buf cnx) portal name params)
   (msg-code-Execute! (cnx-out-buf cnx) portal 0)
   (msg-code-Sync! (cnx-out-buf cnx))
   (cnx-until-ready cnx (filter-row proc) seed))


;; vim: noai ts=3 sw=3 expandtab

